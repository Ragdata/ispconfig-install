#-------------------------------------------------------------------
# functions/dialogs.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         functions/dialogs.sh
# Author:       Ragdata
# Date:         13/01/2021 1526
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
AVUpdateDialog()
{
  CFG_AVUPDATE=$(whiptail --title "Update Freshclam DB" --backtitle "$WT_BACKTITLE" --radiolist "Update AntiVirus Database?" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

CertbotDialog()
{
  CFG_CERTBOT=$(whiptail --title "Install Certbot" --backtitle "$WT_BACKTITLE" --radiolist "Install Let's Encrypt SSL Certbot?" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

HHVMDialog()
{
  CFG_HHVM=$(whiptail --title "Install HHVM" --backtitle "$WT_BACKTITLE" --radiolist "Do you want to install HHVM (Hip Hop Virtual Machine) as PHP engine?" 10 90 2 "Yes" "" OFF "No" "(default)" ON 3>&1 1>&2 2>&3)
}

InitialSetupDialog()
{
  INITIAL=$(whiptail --title "Initial Server Setup" --backtitle "$WT_BACKTITLE" --radiolist "Perform Initial Server Setup?" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

InstallDialog()
{
  INSTALL=$(whiptail --title "ISPConfig Install Type" --backtitle "$WT_BACKTITLE" --radiolist "Select ISPConfig Install Type" 10 90 3 "Full" "Install ISPConfig and ALL Services" OFF "Basic" "ISPConfig + Basic Services (default)" ON "Extras" "Additional Services Install Only (HHVM, XMPP, Passbolt, etc)" OFF 3>&1 1>&2 2>&3)
}

ISPCDialog()
{
  CFG_ISPC=$(whiptail --title "ISPConfig Setup" --backtitle "$WT_BACKTITLE" --radiolist "Select Standard or Expert mode for ISPConfig Install" 10 90 2 "Standard" "(default)" ON "Expert" "" OFF 3>&1 1>&2 2>&3)
}

ISPCVersionDialog()
{
  CFG_ISPCVERSION=$(whiptail --title "ISPConfig Version" --backtitle "$WT_BACKTITLE" --nocancel --radiolist "Select ISPConfig Version you want to install" 10 90 2 "Stable" "(default)" ON "Beta" "" OFF 3>&1 1>&2 2>&3)
}

JailkitDialog()
{
  CFG_JKIT=$(whiptail --title "Install Jailkit" --backtitle "$WT_BACKTITLE" --radiolist "Do you want to install Jailkit?\n(Must be installed before ISPConfig)" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

MailmanDialog()
{
  CFG_MAILMAN=$(whiptail --title "Install Mailman" --backtitle "$WT_BACKTITLE" --radiolist "Install Mailman Mailing List Manager" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

MemDataDialog()
{
  CFG_MEMDATA=$(whiptail --title "In-Memory Data Store" --backtitle "$WT_BACKTITLE" --radiolist "Select In-Memory Data Store" 10 90 3 "Redis" "(default)" ON "Memcached" "" OFF "None" "" OFF 3>&1 1>&2 2>&3)
}

MTADialog()
{
  CFG_MTA=$(whiptail --title "Mail Server" --backtitle "$WT_BACKTITLE" --radiolist "Select Mail Server" 10 90 2 "Dovecot" "(default)" ON "Courier" "" OFF 3>&1 1>&2 2>&3)
}

NextCloudDialog()
{
  CFG_NEXTCLOUD=$(whiptail --title "Install Nextcloud" --backtitle "$WT_BACKTITLE" --radiolist "Do you want to install Nextcloud?" 10 90 2 "Yes" "" ON "No" "(default)" OFF 3>&1 1>&2 2>&3)
}

OpeningDialog()
{
  whiptail --title "ISPConfig Install" --backtitle "$WT_BACKTITLE" --yesno "On the dialog boxes that follow, clicking 'OK' will move you forward to the next question, and clicking 'CANCEL' will move you backwards to the previous question.\n\nAre you ready to begin?" 10 90
}

PasswordDialog()
{
  PASS=$(whiptail --title "Password" --backtitle "$WT_BACKTITLE" --passwordbox "Password for User $USER" --nocancel 10 90 3>&1 1>&2 2>&3)
}

PasswordVaultDialog()
{
  CFG_VAULT=$(whiptail --title "Password Vault" --backtitle "$WT_BACKTITLE" --radiolist "Select Password Vault" 10 90 3 "Passbolt" "" OFF "KeeWeb" "" OFF "None" "" ON 3>&1 1>&2 2>&3)
}

PHPCacheDialog()
{
  CFG_PHPCACHE=$(whiptail --title "PHP Cache" --backtitle "$WT_BACKTITLE" --radiolist "Select PHP Cache\nNOTE: Installing either will cause problems with IonCube" 10 90 3 "OpCache" "" OFF "XCache" "" OFF "None" "(default)" ON 3>&1 1>&2 2>&3)
}

PHPMyAdminDialog()
{
  CFG_PHPMYADMIN=$(whiptail --title "Install phpMyAdmin" --backtitle "$WT_BACKTITLE" --radiolist "Do you want to install phpMyAdmin?" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

QuotaDialog()
{
  CFG_QUOTA=$(whiptail --title "User Quota" --backtitle "$WT_BACKTITLE" --radiolist "Setup User Quota?" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

SSLCountryDialog()
{
  SSL_COUNTRY=$(whiptail --title "SSL Country" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Country Name (2 letter code) (eg: AU)" 10 90 3>&1 1>&2 2>&3)
}

SSLStateDialog()
{
  SSL_STATE=$(whiptail --title "SSL State" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - State or Province Name (Full Name) (eg: Queensland)" 10 90 3>&1 1>&2 2>&3)
}

SSLLocalityDialog()
{
  SSL_LOCALITY=$(whiptail --title "SSL Locality" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Locality Name (eg: City)" 10 90 3>&1 1>&2 2>&3)
}

SSLOrganizationDialog()
{
  SSL_ORGANIZATION=$(whiptail --title "SSL Organization" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Organization Name (eg: Company Pty Ltd)" 10 90 3>&1 1>&2 2>&3)
}

SSLOrgUnitDialog()
{
  SSL_ORGUNIT=$(whiptail --title "SSL Organization Unit" --backtitle "$WT_BACKTITLE" --inputbox "SSL Configuration - Organizational Unit Name (eg: IT Department)" 10 90 3>&1 1>&2 2>&3)
}

SSODialog()
{
  CFG_GLUU=$(whiptail --title "SSO Server" --backtitle "$WT_BACKTITLE" --radiolist "Install Gluu Identity Manager / Single Sign-On (SSO) Server?" 10 90 2 "Yes" "" OFF "No" "(default)" ON 3>&1 1>&2 2>&3)
}

SQLServerDialog()
{
  CFG_SQLSERVER=$(whiptail --title "SQL Server" --backtitle "$WT_BACKTITLE" --radiolist "Select SQL Server" 10 90 2 "MySQL" "" OFF "MariaDB" "(default)" ON 3>&1 1>&2 2>&3)
}

SQLServerPasswordDialog()
{
  CFG_MYSQL_ROOT_PWD=$(whiptail --title "$CFG_SQLSERVER Root Password" --backtitle "$WT_BACKTITLE" --passwordbox "Specify a root SQL Server password" 10 90 3>&1 1>&2 2>&3)
}

UserDialog()
{
  USER=$(whiptail --title "SUDO User" --backtitle "$WT_BACKTITLE" --inputbox "SUDO Username" --nocancel 10 90 3>&1 1>&2 2>&3)
}

WebMailDialog()
{
  CFG_WEBMAIL=$(whiptail --title "Webmail Package" --backtitle "$WT_BACKTITLE" --radiolist "Select Webmail Package" 10 90 3 "RoundCube" "(default)" ON "SquirrelMail" "" OFF "None" "" OFF 3>&1 1>&2 2>&3)
}

WebServerDialog()
{
  CFG_WEBSERVER=$(whiptail --title "Web server" --backtitle "$WT_BACKTITLE" --radiolist "Select Web Server" 10 90 2 "Apache" "(default)" ON "Nginx" "" OFF 3>&1 1>&2 2>&3)
}

WebDAVDialog()
{
  CFG_WEBDAV=$(whiptail --title "WebDAV Server" --backtitle "$WT_BACKTITLE" --radiolist "Configure WebDAV Server" 10 90 2 "Yes" "(default)" ON "No" "" OFF 3>&1 1>&2 2>&3)
}

XMPPDialog()
{
  CFG_XMPP=$(whiptail --title "XMPP Server" --backtitle "$WT_BACKTITLE" --radiolist "Select XMPP Server" 10 90 3 "Jabberd" "(default)" ON "Metronome" "" OFF "None" "" OFF 3>&1 1>&2 2>&3)
}