#-------------------------------------------------------------------
# functions/functions.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         functions/functions.sh
# Author:       Ragdata
# Date:         13/01/2021 1724
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------

function hide_output {
  red='\e[0;31m'
  yellow='\e[0;33m'
  NC='\e[0m'

  OUTPUT=$(mktemp)

  "$@" &> "$OUTPUT"

  E=$?
  if [[ $E -ne 0 ]]; then
    echo -e "\n${red}ERROR: The following command failed: $*${NC}\n"
    echo "${red}--------------------------------------------${NC}"
    echo "${yellow}$OUTPUT${NC}"
    echo "${red}--------------------------------------------${NC}"
    echo
    rm -f "$OUTPUT"
    exit $E
  fi

  rm -f "$OUTPUT"
}

function pkg_install {
  if echo "$ID" | grep -iq "debian\|ubuntu\|raspbian"; then
    hide_output apt install -y "$@"
  else
    hide_output yum install -y "$@"
  fi
}

function pkg_remove {
  if echo "$ID" | grep -iq "debian\|ubuntu\|raspbian"; then
    hide_output apt remove -y "$@"
  else
    hide_output yum erase -y "$@"
  fi
}

function exitRedirect {
  NEXT=$1
  PREV=$2
  STATUS=$3
  if [ "$STATUS" == 0 ]; then
    $NEXT
  else
    $PREV
  fi
}

function InitialServerSetup {
  UserDialog
  if [ ! -d "/home/$USER" ]; then
    PasswordDialog
    useradd "$USER" -p "$PASS"
    usermod -aG sudo "$USER"
    rsync --archive --chown="$USER:$USER" ~/.ssh /home/"$USER"
  fi
}