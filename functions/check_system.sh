#-------------------------------------------------------------------
# functions/check_system.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         functions/check_system.sh
# Author:       Ragdata
# Date:         13/01/2021 0828
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
CheckSystem() {

  echo -n "Starting system scan ... "
  # Extract System Information
  . /etc/os-release

  DISTRO=''

  if echo "$ID" | grep -iq "ubuntu"; then

    if echo "$VERSION_ID" | grep -iq "14.04"; then
      DISTRO=ubuntu-14.04

    elif echo "$VERSION_ID" | grep -iq "15.10"; then
      DISTRO=ubuntu-15.10

    elif echo "$VERSION_ID" | grep -iq "16.04"; then
      DISTRO=ubuntu-16.04

    elif echo "$VERSION_ID" | grep -iq "16.10"; then
      DISTRO=ubuntu-16.10

    elif echo "$VERSION_ID" | grep -iq "17.10"; then
      DISTRO=ubuntu-17.10

    elif echo "$VERSION_ID" | grep -iq "18.04"; then
      DISTRO=ubuntu-18.04

    elif echo "$VERSION_ID" | grep -iq "20.04"; then
      DISTRO=ubuntu-20.04
    fi

  elif echo "$ID" | grep -iq "debian"; then

    if echo "$VERSION_ID" | grep -iq "7"; then
      DISTRO=ubuntu-20.04

    elif echo "$VERSION_ID" | grep -iq "8"; then
      DISTRO=ubuntu-20.04

    elif echo "$VERSION_ID" | grep -iq "9"; then
      DISTRO=ubuntu-20.04

    elif echo "$VERSION_ID" | grep -iq "10"; then
      DISTRO=ubuntu-20.04
    fi

  elif echo "$ID" | grep -iq "raspbian"; then
    DISTRO=Raspbian

  elif echo "$ID" | grep -iq "centos"; then
    DISTRO=CentOS

  elif echo "$ID" | grep -iq "opensuse"; then
    DISTRO=OpenSUSE

  elif echo "$ID" | grep -iq "fedora"; then
    DISTRO=Fedora

  fi

  echo -e "[${green}DONE${NC}]\n"
}