# ISPConfig-Install
[![Build Status](https://gitlab.com/Ragdata/ispconfig-install/badges/master/pipeline.svg)](https://gitlab.com/Ragdata/ispconfig-install/tree/master)

Autoinstaller for ISPConfig 3+ which includes a host of software packages and configuration tweaks that are essential for any modern Shared Hosting setup based on ISPConfig 3+

## Installation

There are actually a few versions of this script available to you.  In each case, the best way to install is to clone the repo and run the appropriate script.

### Full Version
The Full Version of the script installs EVERYTHING you might need
````
cd /tmp
git clone git@gitlab.com:Ragdata/ispconfig-install.git
cd ispconfig-install
bash install.sh
````

### Basic Version
The Basic Version installs ONLY what is required by ISPConfig
````
cd /tmp
git clone git@gitlab.com:Ragdata/ispconfig-install.git
cd ispconfig-install
bash basic_install.sh
````

### Extras Version
The Extras Version assumes that you've installed everything included in the Basic Version and provides options to install those extra packages that really pack out the capabilities of a server
````
cd /tmp
git clone git@gitlab.com:Ragdata/ispconfig-install.git
cd ispconfig-install
bash extras_install.sh
````

## Version History
[Semantic Versioning 2.0](https://semver.org/)
****
### Current Version:
#### v1.0.1

### Version History:
#### v.1.0.2:    Extras Version Included
* Supports Ubuntu 20.04 only

#### v.1.0.1:    Basic Version Included
* Supports Ubuntu 20.04 only

#### v.1.0.0:    First Full Version
* Supports Ubuntu 20.04 only

#### v.0.0.1:    Initial Commit

## Contributors
****
* Developer of this version: Ragdata <ragdata@users.noreply.gitlab.com>
* Based on the work of Matteo Temporini <temporini.matteo@gmail.com>
* And, of course, the members of the [ISPConfig Autoinstaller](https://git.ispconfig.org/ispconfig/ispconfig-autoinstaller) project