#!/usr/bin/env bash
#-------------------------------------------------------------------
# install.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         install.sh
# Author:       Ragdata
# Date:         13/01/2021 0758
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
# BASH COLOURS
red='\e[0;31m'
green='\e[0;32m'
yellow='\e[0;33m'
bold='\e[1m'
underlined='\e[4m'
NC='\e[0m' # No Color
COLUMNS=$(tput cols)

clear

# Check if user running script is root
if [[ $(id -u) -ne 0 ]]; then
  echo -e "${red}ERROR: This script must be run as root${NC}" >&2
  exit 1
fi

# Check if on Linux
if ! echo "$OSTYPE" | grep -iq "linux"; then
  echo -e "${red}ERROR: This script must be run on Linux${NC}" >&2
  exit 1
fi

# Check RAM
TOTAL_MEM=$(awk '/^MemTotal:/ {print $2}' /proc/meminfo)
TOTAL_SWAP=$(awk '/^SwapTotal:/ {print $2}' /proc/meminfo)
PRINT_RAM_MiB=$(printf "%'d" $((TOTAL_MEM / 1024)))
PRINT_RAM_MB=$(printf "%'d" $((((TOTAL_MEM * 1024) / 1000) / 1000)))

if [ "$TOTAL_MEM" -lt 524288 ]; then
  echo "This machine has: $PRINT_RAM_MiB MiB (PRINT_RAM_MB) RAM (memory)"
  echo -e "\n${red}ERROR: ISPConfig needs at least 512 MiB RAM, with 1 GiB (1024 MiB) recommended"
  exit 1
fi

# Check Connectivity
echo -n "Checking internet connection ... "

if ! ping -q -c 3 www.ispconfig.org > /dev/null 2>&1; then
  echo -e "${red}ERROR: Could not reach ispconfig.org - please check your internet connection and run the script again${NC}" >&2
  exit 1
fi

echo -e "[${green}DONE${NC}]\n"

clear

# Check if ISPConfig is already installed
# TODO - In future, I'll make this an upgrade pathway
if [ -f /usr/local/ispconfig/interface/lib/config.inc.php ]; then
  echo -e "${red}ERROR: ISPConfig is already installed${NC}" >&2
  echo 1
fi

clear

#-------------------------------------------------------------------
# GLOBAL VARIABLES
#-------------------------------------------------------------------
CFG_HOSTNAME_FQDN=$(hostname -f);
IP_ADDRESS=( $(hostname -I) );
RE='^2([0-4][0-9]|5[0-5])|1?[0-9][0-9]{1,2}(\.(2([0-4][0-9]|5[0-5])|1?[0-9]{1,2})){3}$'
IPv4_ADDRESS=( $(for i in ${IP_ADDRESS[*]}; do [[ "$i" =~ $RE ]] && echo "$i"; done) )
RE='^[[:xdigit:]]{1,4}(:[[:xdigit:]]{1,4}){7}$'
IPv6_ADDRESS=( $(for i in ${IP_ADDRESS[*]}; do [[ "$i" =~ $RE ]] && echo "$i"; done) )
WT_BACKTITLE="ISPConfig 3+ Installer"
# Save Current Working Directory
APWD=$(pwd);
#-------------------------------------------------------------------
# LOAD FUNCTIONS
#-------------------------------------------------------------------
source $APWD/functions/check_system.sh
echo -n "Checking your system, please wait ... "
CheckSystem
echo -e "[${green}DONE${NC}]\n"

clear

RE='^.+\.localdomain$'
RE1='^.{4,253}$'
RE2='^([[:alnum:]][[:alnum:]\-]{0,61}[[:alnum:]]\.)+[a-zA-Z]{2,63}$'

if [[ $CFG_HOSTNAME_FQDN =~ $RE ]]; then
  echo -e "${yellow}WARNING: Invalid Hostname! Hostname cannot be *.localdomain${NC}\n"
elif ! [[ $CFG_HOSTNAME_FQDN =~ $RE1 && $CFG_HOSTNAME_FQDN =~ $RE2 ]]; then
  echo -e "${yellow}WARNING: Invalid Hostname! Hostname is not a fully-qualified domain name (FQDN)${NC}"
fi

if [[ $CFG_HOSTNAME_FQDN =~ $RE ]] || ! [[ $CFG_HOSTNAME_FQDN =~ $RE1 && $CFG_HOSTNAME_FQDN =~ $RE2 ]]; then
  echo -e "Your IP Address is: ${IP_ADDRESS[0]}\n"
  echo -e "${yellow}WARNING: If this system is connected to a router and/or behind a NAT, please ensure that the private (internal) IP is static before continuing.${NC}"
  echo -e "For most routers, static internal IP addresses are usually assigned via DHCP reservation. See your router's guide for more info.\nYou will also need to forward some ports depending upon the software you choose to install, and whether or not you want these to be available to the outside world."
  echo -e "\n\tTCP Ports\n\t\t20\t- FTP\n\t\t21\t- FTP\n\t\t22\t- SSH/SFTP\n\t\t25\t- Mail (SMTP)\n\t\t53\t- DNS\n\t\t80\t- Web (HTTP)\n\t\t110\t- Mail (POP3)\n\t\t143\t- Mail (IMAP)\n\t\t443\t- Web (HTTPS)\n\t\t465\t- Mail (SMTPS)\n\t\t587\t- Mail (SMTP)\n\t\t993\t- Mail (IMAPS)\n\t\t995\t- Mail (POP3S)\n\t\t3306\t- Database\n\t\t5222\t- Chat (XMPP)\n\t\t8080\t- ISPConfig\n\t\t8081\t- ISPConfig\n\t\t10000\t- ISPConfig\n\n\tUDP Ports\n\t\t53\t- DNS\n\t\t3306\t- Database\n" |fold -s -w "$COLUMNS"
  echo -n "Would you like to update the hostname for this system? (Recommended) (y/n)"
  read -n 1 -r
  echo -e "\n"
  RE='^[Yy]$'
  if [[ $REPLY =~ $RE ]]; then
    while ! [[ $line =~ $RE1 && $line =~ $RE2 ]]; do
      echo -n "Please enter a fully-qualified domain name (FQDN) (eg: ${HOSTNAME%%.*}.example.com):"
      read -r line
    done
    hostnamectl set-hostname "$line"
    if grep -q "^${IP_ADDRESS[0]}" /etc/hosts; then
      sed -i "s/^${IP_ADDRESS[0]}.*/${IP_ADDRESS[0]}\t$line/" /etc/hosts
    else
      sed -i "s/^127.0.0.1/${IP_ADDRESS[0]}\t$line/" /etc/hosts
    fi
    CFG_HOSTNAME_FQDN=$(hostname -f);
  fi
fi

# SUPPORTED DISTRO CHECK
if [ ! -d $APWD/distros/$DISTRO ]; then
  echo -e "${red}ERROR: $DISTRO is not currently supported! Please check back with us soon${NC}"
  exit 1
fi

#-------------------------------------------------------------------
# MAIN PROGRAM LOOP [ main() ]
#-------------------------------------------------------------------
clear

CPU=( $(sed -n 's/^model name[[:space:]]*: *//p' /proc/cpuinfo | uniq) )
CORES=$(nproc --all)
ARCHITECTURE=$(getconf LONG_BIT)
SWAP_MiB=$(printf "%'d" $((TOTAL_SWAP / 1024)))
SWAP_MB=$(printf "%'d" $((((TOTAL_SWAP * 1024) / 1000) / 1000)))
if command -v lspci >/dev/null; then
  GPU=( $(lspci 2>/dev/null | grep -i 'vga\|3d\|2d' | sed -n 's/^.*: //p') )
fi
TIME_ZONE="$(cat /etc/timezone) $(date +"(%Z, %z)")"

echo "=============================================================="
echo "Welcome to ISPConfig AutoInstall v1.0.0!"
echo "=============================================================="
echo -e "This script will perform an ALMOST unattended installation of"
echo "all the software you need to run ISPConfig 3+ and a little more!"
echo "Please ensure the following before you run the script:"
echo -e "\t- that this is a clean installation of your distro (brand new);"
echo -e "\t- that this script supports your distro;"
echo -e "\t\t- currently, that's only Ubuntu 20.04, but I'm working on more ..."
echo -e "\t- that your internet connection is working and stable;"
echo -e "\t- that the following information is what you expected to see:\n"
echo -e "\t\tDetected Linux Distro:\t${PRETTY_NAME:-$ID-$VERSION_ID}\n"
if [ -n "$ID_LIKE" ]; then
  echo -e "Related Linux Distros:\t\t\t$ID_LIKE"
fi
if [ -n "$CPU" ]; then
  echo -e "Processor (CPU):\t\t\t${CPU[*]}"
fi
echo -e "CPU Cores:\t\t\t\t$CORES"
echo -e "Architecture:\t\t\t\t$HOSTTYPE ($ARCHITECTURE-bit)"
echo -e "TOTAL RAM (memory):\t\t\t$PRINT_RAM_MiB MiB ($PRINT_RAM_MB MB)"
echo -e "Total Swap Space:\t\t\t$SWAP_MiB MiB ($SWAP_MB MB)"
if [ -n "$GPU" ]; then
  echo -e "Graphics Processor (GPU):\t\t${GPU[*]}"
fi
echo -e "Computer Name:\t\t\t\t$HOSTNAME"
echo -e "Hostname:\t\t\t\t$CFG_HOSTNAME_FQDN"
if [ -n "$IPv4_ADDRESS" ]; then
  echo -e "IPv4 Address$([[ ${#IPv4_ADDRESS[*]} -gt 1 ]] && echo "es"):\t\t\t\t${IPv4_ADDRESS[*]}"
fi
if [ -n "$IPv6_ADDRESS" ]; then
  echo -e "IPv6 Address$([[ ${#IPv6_ADDRESS[*]} -gt 1 ]] && echo "es"):\t\t\t\t${IPv6_ADDRESS[*]}"
fi
echo -e "Timezone:\t\t\t\t$TIME_ZONE\n"
if CONTAINER=$(systemd-detect-virt -c); then
	echo -e "Virtualization container:\t\t$CONTAINER\n"
fi
if VM=$(systemd-detect-virt -v); then
	echo -e "Virtual Machine (VM) hypervisor:\t$VM\n"
fi
if uname -r | grep -iq "microsoft"; then
	echo -e "${yellow}Warning: The Windows Subsystem for Linux (WSL) is not yet fully supported by this script.${NC}"
	echo -e "For more information, see this issue: https://github.com/servisys/ispconfig_setup/issues/176\n"
fi

if [ -n "$DISTRO" ]; then
  echo -e "Installing for the following Linux Distribution:\t$DISTRO"
  echo -n "Is this correct? (y/n)"
  read -n 1 -r
  echo -e "\n"
  RE='^[Yy]$'
  if [[ ! $REPLY =~ $RE ]]; then
    exit 1
  fi
else
  echo -e "Sorry, but your system is not supported by this script. If you'd like me to support your system, " >&2
  echo -e "open an issue on GitHub: https://github.com/Ragdata/ISPConfig-AutoInstall/issues" >&2
  if echo "$ID" | grep -iq 'ubuntu'; then
    echo -e "\nIt is possible that this script will work if you manually set the DISTRO variable to a version of $ID that is supported"
  elif [ -n "$ID_LIKE" ] && echo "$ID_LIKE" | grep -iq 'ubuntu'; then
    echo -e "\nIt is possible that this script will work if you manually set the DISTRO variable to one of the related Linux distributions that is supported"
  fi
  if echo "$ID" | grep -iq "OpenSUSE"; then
    echo -e "\nYou can use the script here while it lasts: https://gist.github.com/jniltinho/7734f4879c4469b9a47f3d3eb4ff0bfb"
    echo -e "You'll stand an even better chance if you can modify it to suit your version of $ID"
    echo -e "You may want to take a look at this issue though: https://git.ispconfig.org/ispconfig/ispconfig3/issues/5074"
  fi
  exit 1
fi

source "$APWD"/functions/check_system.sh
source "$APWD"/functions/functions.sh
source "$APWD"/functions/dialogs.sh

source "$APWD"/install_packages.sh

RE='^.*[^[:space:]]+.*$'

# Install Whiptail
if ! command -v whiptail >/dev/null; then
  echo -n "Installing Whiptail ... "
  apt install -y whiptail
  echo -e "[${green}DONE${NC}]\n"
fi

CheckSystem
InitialServerSetup
InstallDialog

if [ "$INSTALL" == "Full" ]; then
  source "$APWD"/distros/"$DISTRO"/askquestions.sh
  AskQuestions
elif [ "$INSTALL" == "Basic" ]; then
  source "$APWD"/distros/"$DISTRO"/ask_basic_questions.sh
  AskBasicQuestions
elif [ "$INSTALL" == "Extras" ]; then
  source "$APWD"/distros/"$DISTRO"/ask_extras_questions.sh
  AskExtrasQuestions
fi
