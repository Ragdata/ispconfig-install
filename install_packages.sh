#-------------------------------------------------------------------
# install_packages.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         install_packages.sh
# Author:       Ragdata
# Date:         13/01/2021 0758
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallPackages()
{
  if [ -f /etc/debian_version ]; then

    if [ "$DISTRO" == "debian8" ]; then
      ISPCVersionDialog
    fi

    if [ "$INSTALL" == "Full" ] || [ "$INSTALL" == "Basic" ]; then

      if [ ! -x /usr/sbin/mysqld ]; then
        source "$APWD"/distros/"$DISTRO"/install_basics.sh
        source "$APWD"/distros/"$DISTRO"/install_sql.sh
        InstallBasics
        InstallSQLServer
      fi

      if [ "$CFG_SETUP_WEB" == "Yes" ] || [ "$CFG_MULTISERVER" == "No" ]; then

        if [ ! -x /usr/sbin/nginx ]; then
          source "$APWD"/distros/"$DISTRO"/install_webserver.sh
          InstallWebServer
        fi

        if [ ! -x /usr/sbin/pure-ftpd-control ]; then
          source "$APWD"/distros/"$DISTRO"/install_ftp.sh
          InstallFTP
        fi

#        source "$APWD"/distros/"$DISTRO"/install_webdav.sh
#        InstallWebDAV

      else
        source "$APWD"/distros/"$DISTRO"/install_basephp.sh
        InstallBasePHP
      fi

      if [ "$CFG_SETUP_MAIL" == "Yes" ] || [ "$CFG_MULTISERVER" == "No" ]; then

        if [ ! -x /usr/sbin/postfix ]; then
          source "$APWD"/distros/"$DISTRO"/install_postfix.sh
          InstallPostfix
        fi

        if [ ! -x /usr/sbin/dovecot ]; then
          source "$APWD"/distros/"$DISTRO"/install_mta.sh
          InstallMTA
#        else
#          apt remove -y opendkim opendkim-tools opendmarc
#          apt install -y opendkim opendkim-tools opendmarc
        fi

        if [ ! -x /usr/bin/freshclam ]; then
          source "$APWD"/distros/"$DISTRO"/install_antivirus.sh
          InstallAntiVirus
        fi

      fi

      if [ "$CFG_MAILMAN" != "No" ]; then
        if [ ! -d /etc/mailman ]; then
          source "$APWD"/distros/"$DISTRO"/install_mailman.sh
          InstallMailman
        fi
      fi

      if [ "$CFG_SETUP_DNS" == "Yes" ] || [ "$CFG_MULTISERVER" == "No" ]; then
        if [ ! -d /etc/bind ]; then
          source "$APWD"/distros/"$DISTRO"/install_dns.sh
          InstallBind
        fi
      fi

      if [ "$CFG_QUOTA" == "Yes" ]; then
        if [ ! -x /usr/bin/quota ]; then
          source "$APWD"/distros/"$DISTRO"/install_quota.sh
          InstallQuota
        fi
      fi

      if [ "$CFG_JKIT" == "Yes" ]; then
        source "$APWD"/distros/"$DISTRO"/install_jailkit.sh
        InstallJailKit
      fi
    fi

    if [ "$INSTALL" == "Full" ] || [ "$INSTALL" == "Extras" ]; then

      if [ "$CFG_HHVM" == "Yes" ]; then
        source "$APWD"/distros/"$DISTRO"/install_hhvm.sh
        InstallHHVM
      fi

  #    if [ "$CFG_MEMDATA" != "No" ]; then
  #      source $APWD/distros/$DISTRO/install_memdata.sh
  #      InstallDataStore
  #    fi

    fi

    if [ "$INSTALL" == "Full" ] || [ "$INSTALL" == "Basic" ]; then

      source "$APWD"/distros/"$DISTRO"/install_webstats.sh
      source "$APWD"/distros/"$DISTRO"/install_firewall.sh
      InstallWebStats
      InstallFail2Ban

      if [ "$CFG_ISPCVERSION" == "Beta" ]; then
        source "$APWD"/distros/"$DISTRO"/install_ispconfigbeta.sh
        InstallISPConfigBeta
      fi

      source "$APWD"/distros/"$DISTRO"/install_ispconfig.sh
      source "$APWD"/distros/"$DISTRO"/install_dkim.sh
      InstallISPConfig
      InstallDKIM

      if [ "$CFG_WEBMAIL" != "No" ]; then
        source "$APWD"/distros/"$DISTRO"/install_webmail.sh
        InstallWebmail
      fi

    fi

    if [ "$INSTALL" == "Full" ] || [ "$INSTALL" == "Extras" ]; then

      if [ "$CFG_XMPP" != "No" ]; then
        source "$APWD"/distros/"$DISTRO"/install_xmpp.sh
        InstallXMPP
      fi

      if [ "$CFG_GLUU" != "No" ]; then
        source "$APWD"/distros/"$DISTRO"/install_gluu.sh
        InstallGluu
      fi

      if [ "$CFG_NEXTCLOUD" != "No" ]; then
        source "$APWD"/distros/"$DISTRO"/install_nextcloud.sh
        InstallNextCloud
      fi

      if [ "$CFG_VAULT" != "No" ]; then
        source "$APWD"/distros/"$DISTRO"/install_passwordvault.sh
        InstallPasswordVault
      fi

    fi

    CFG_CERTBOT=$(apt-cache policy certbot | grep -q Installed)

    if [ -z "$CFG_CERTBOT" ]; then
      source "$APWD"/distros/"$DISTRO"/install_certbot.sh
      InstallCertbot
    fi

    source "$APWD"/distros/"$DISTRO"/install_certs.sh
    CheckInstallCertificates

    echo -e "\n${green}Well done !! ISPConfig installed and running !!${NC} 😃"
    echo -e "\nNow you can access to your ISPConfig installation at: ${underlined}https://$CFG_HOSTNAME_FQDN:8080${NC} or ${underlined}https://${IP_ADDRESS[0]}:8080${NC}"
    echo -e "The default ISPConfig Username is: ${bold}admin${NC}\n\t      and the Password is: ${bold}admin${NC}"
    echo -e "${yellow}Warning: This is a security risk. Please change the default password after your first login.${NC}"

    if [ "$CFG_WEBSERVER" == "Nginx" ]; then
      if [ "$CFG_PHPMYADMIN" == "yes" ]; then
        echo "phpMyAdmin is accessible at: http://$CFG_HOSTNAME_FQDN:8081/phpmyadmin or http://${IP_ADDRESS[0]}:8081/phpmyadmin";
      fi
      if [ "$DISTRO" == "debian8" ] && [ "$CFG_WEBMAIL" == "roundcube" ]; then
        echo "Webmail is accessible at: https://$CFG_HOSTNAME_FQDN/webmail or https://${IP_ADDRESS[0]}/webmail";
      else
        echo "Webmail is accessible at: http://$CFG_HOSTNAME_FQDN:8081/webmail or http://${IP_ADDRESS[0]}:8081/webmail";
      fi
    fi

  fi
}