#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_passwordvault.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_passwordvault.sh
# Author:       Ragdata
# Date:         13/01/2021 1853
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallPasswordVault() {

  if [ "$CFG_VAULT" == "Passbolt" ]; then

    mkdir /tmp/passbolt
    cd passbolt

    wget https://github.com/passbolt/passbolt_install_scripts/archive/master.zip

    unzip master.zip

  elif [ "$CFG_VAULT" == "KeeWeb" ]; then

    echo "HERE"

  fi

}