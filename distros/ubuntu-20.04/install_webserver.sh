#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_webserver.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_webserver.sh
# Author:       Ragdata
# Date:         13/01/2021 1835
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallWebServer() {

  if [ "$CFG_WEBSERVER" == "Apache" ]; then

    CFG_NGINX=n
    CFG_APACHE=y

    echo -n "Installing Web Server (Apache) and modules ... "
    echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
    echo "phpmyadmin phpmyadmin/dbconfig-install boolean false" | debconf-set-selections
    echo "dbconfig-common dbconfig-common/dbconfig-install boolean false" | debconf-set-selections

    apt install -y apache2 apache2-doc apache2-utils libapache2-mod-php libapache2-mod-fcgid apache2-suexec-pristine libruby libapache2-mod-python

    echo -e "[${green}DONE${NC}]\n"

    echo -n "Installing PHP7.4 and modules ... "
    source $APWD/distros/$DISTRO/php7.4.sh
    InstallPHP74
    apt install -y php-apcu
    echo -e "[${green}DONE${NC}]\n"

    echo -n "Installing PHP-FPM ... "
    apt install -y php7.4-fpm
    echo -e "[${green}DONE${NC}]\n"

    if [ "$CFG_PHPMYADMIN" == "yes" ]; then
      echo "=============================================================================================="
      echo "Attention: When asked to 'Configure database for phpmyadmin with dbconfig-common?' ${yellow}select 'NO'${NC}"
      echo "Due to a bug in dbconfig-common, this can't be automated!"
      echo "=============================================================================================="
      echo "Press ENTER to continue ... "
      read DUMMY
      echo -n "Installing phpMyAdmin ... "
      apt install -y phpmyadmin
      echo -e "[${green}DONE${NC}]\n"
    fi

    echo -n "Activating Apache Modules ... "
    a2enmod suexec > /dev/null 2>&1
    a2enmod rewrite > /dev/null 2>&1
    a2enmod ssl > /dev/null 2>&1
    a2enmod actions > /dev/null 2>&1
    a2enmod include > /dev/null 2>&1
    a2enmod cgi > /dev/null 2>&1
    a2enmod dav_fs > /dev/null 2>&1
    a2enmod dav > /dev/null 2>&1
    a2enmod auth_digest > /dev/null 2>&1
	  a2enmod fastcgi > /dev/null 2>&1
	  a2enmod proxy_fcgi > /dev/null 2>&1
	  a2enmod alias > /dev/null 2>&1
	  echo -e "[${green}DONE${NC}]\n"

	  echo -n "Restarting Apache... "
	  service apache2 restart
	  echo -e "[${green}DONE${NC}]\n"

  elif [ "$CFG_WEBSERVER" == "Nginx" ]; then

    CFG_NGINX=y
    CFG_APACHE=n

    echo -n "Installing Web Server (Nginx) and modules ... "
    service apache2 stop
    hide_output update-rc.d -f apache2 remove
    apt install -y nginx-full
    service nginx start
    echo -e "[${green}DONE${NC}]\n"

    echo -n "Installing PHP-FPM... "
    apt install -y php7.4-fpm
    echo -e "[${green}DONE${NC}]\n"

    echo -e "Installing PHP and modules ... "
    source $APWD/distros/$DISTRO/php7.4.sh
    InstallPHP74
    echo -e "[${green}DONE${NC}]\n"

  	echo -n "Reloading PHP-FPM... "
	  service php7.4-fpm reload
  	echo -e "[${green}DONE${NC}]\n"

	  echo -n "Installing fcgiwrap... "
	  apt install -y fcgiwrap
	  echo -e "[${green}DONE${NC}]\n"

    if [ "$CFG_PHPMYADMIN" == "yes" ]; then
      echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
      # - DISABLED DUE TO A BUG IN DBCONFIG - echo "phpmyadmin phpmyadmin/dbconfig-install boolean false" | debconf-set-selections
      echo "dbconfig-common dbconfig-common/dbconfig-install boolean false" | debconf-set-selections
      echo -n "Installing phpMyAdmin... "
      apt install -y phpmyadmin
      echo "With nginx phpMyAdmin is accessibile at  http://$CFG_HOSTNAME_FQDN:8081/phpmyadmin or http://${IP_ADDRESS[0]}:8081/phpmyadmin"
      echo -e "[${green}DONE${NC}]\n"
    fi

  fi

}