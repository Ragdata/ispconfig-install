#-------------------------------------------------------------------
# distros/ubuntu-20.04/preinstallcheck.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/preinstallcheck.sh
# Author:       Ragdata
# Date:         13/01/2021 1527
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
PreInstallCheck() {
  echo -n "Preparing to install ... "
  # Set Working Variables
  REL=$(lsb_release -cs)
  BACKPORTS="${REL}-backports"
  ALL_REPOS=$(grep "$BACKPORTS main restricted universe multiverse" /etc/apt/sources.list)
  DIGI_OCEAN=$(grep "digitalocean.com" /etc/apt/sources.list)
  # Check sources.list
#  if [ -z "$ALL_REPOS" ]; then
#    if [ -z "$DIGI_OCEAN" ]; then
#      REPO_DOMAIN="mirrors.digitalocean.com";
#    else
#      REPO_DOMAIN="archive.ubuntu.com";
#    fi
#    echo "deb http://${REPO_DOMAIN}/ubuntu ${BACKPORTS} main restricted universe multiverse"
#  else
#    sed -i 's/main/main restricted universe multiverse/' /etc/apt/sources.list;
#  fi
  CFG_MULTISERVER=no
}