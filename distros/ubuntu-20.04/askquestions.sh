#-------------------------------------------------------------------
# distros/ubuntu-20.04/askquestions.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/askquestions.sh
# Author:       Ragdata
# Date:         13/01/2021 1526
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
AskQuestions()
{
  CFG_MULTISERVER=No
  CFG_WEBMAIL=RoundCube
  OpeningDialog
  exitstatus=$?
  if [ $exitstatus == 0 ]; then
    SQLServerQuestion
  else
    exit 1
  fi
}

SQLServerQuestion()
{
  SQLServerDialog
  exitRedirect SQLServerPasswordQuestion AskQuestions $?
}

SQLServerPasswordQuestion()
{
  SQLServerPasswordDialog
  exitRedirect WebServerQuestion SQLServerQuestion $?
}

WebServerQuestion()
{
  WebServerDialog
  exitRedirect PHPCacheQuestion SQLServerPasswordQuestion $?
}

WebDAVQuestion()
{
  WebDAVDialog
  exitRedirect PHPCacheQuestion WebServerQuestion $?
}

PHPCacheQuestion()
{
  PHPCacheDialog
  exitRedirect MTAQuestion WebDAVQuestion $?
}

MTAQuestion()
{
  MTADialog
  exitRedirect MailmanQuestion PHPCacheQuestion $?
}

MailmanQuestion()
{
  MailmanDialog
  exitRedirect HHVMQuestion MTAQuestion $?
}

HHVMQuestion()
{
  HHVMDialog
  exitRedirect XMPPQuestion MTAQuestion $?
}

XMPPQuestion()
{
  XMPPDialog
  exitRedirect MemDataQuestion HHMVQuestion $?
}

MemDataQuestion()
{
  MemDataDialog
  exitRedirect SSOQuestion XMPPQuestion $?
}

SSOQuestion()
{
  SSODialog
  exitRedirect AVUpdateQuestion MemDataQuestion $?
}

AVUpdateQuestion()
{
  AVUpdateDialog
  exitRedirect QuotaQuestion SSOQuestion $?
}

QuotaQuestion()
{
  QuotaDialog
  exitRedirect JailkitQuestion AVUpdateQuestion $?
}

JailkitQuestion()
{
  JailkitDialog
  exitRedirect ISPCQuestion QuotaQuestion $?
}

ISPCQuestion()
{
  ISPCDialog
  exitRedirect PHPMyAdminQuestion JailkitQuestion $?
}

PHPMyAdminQuestion()
{
  PHPMyAdminDialog
  exitRedirect PasswordVaultQuestion ISPCQuestion $?
}

PasswordVaultQuestion()
{
  PasswordVaultDialog
  exitRedirect NextCloudQuestion PHPMyAdminQuestion $?
}

NextCloudQuestion()
{
  NextCloudDialog
  exitRedirect CertbotQuestion PasswordVaultQuestion $?
}

CertbotQuestion()
{
  CertbotDialog
  exitRedirect SSLCountryQuestion NextCloudQuestion $?
}

SSLCountryQuestion()
{
  SSLCountryDialog
  exitRecirect SSLStateQuestion NextcloudQuestion $?
}

SSLStateQuestion()
{
  SSLStateDialog
  exitRedirect SSLLocalityQuestion SSLCountryQuestion $?
}

SSLLocalityQuestion()
{
  SSLLocalityDialog
  exitRedirect SSLOrganizationQuestion SSLStateQuestion $?
}

SSLOrganizationQuestion()
{
  SSLOrganizationDialog
  exitRedirect SSLOrgUnitQuestion SSLLocalityQuestion $?
}

SSLOrgUnitQuestion()
{
  SSLOrgUnitDialog
  exitRedirect InstallPackages SSLOrganizationQuestion $?
}
