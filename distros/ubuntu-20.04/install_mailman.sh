#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_mailman.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_mailman.sh
# Author:       Ragdata
# Date:         13/01/2021 1803
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallMailman()
{
  echo -n "Installing Mailman Mailing List Manager ... "
  apt install -y mailman
  echo -e "[${green}DONE${NC}]\n"
  service postfix restart
  service mailman start
}