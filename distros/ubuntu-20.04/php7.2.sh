#-------------------------------------------------------------------
# distros/ubuntu-20.04/php7.2.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/php7.2.sh
# Author:       Ragdata
# Date:         14/01/2021 0031
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallPHP72() {

  echo -n "Installing PHP7.2 ... "
  apt install -y php7.2 php7.2-{amqp,apcu,bcmath,bz2,cgi,cli,common,curl,ds,gd,geoip,gmp,gnupg,http,imagick,imap,intl,json,ldap,lua,mailparse,mbstring,oauth,odbc,pcov,pgsql,pspell,psr,readline,recode,redis,sqlite3,ssh2,stomp,tideways,tidy,uploadprogress,uuid,xhprof,xml,xmlrpc,yaml,zip,zmq}
  echo -e "[${green}DONE${NC}]\n"

  if [ "$CFG_MEMDATA" != "no" ]; then
    if [ "$CFG_MEMDATA" == "Redis" ]; then
      apt install -y php7.2-redis
    else
      apt install -y php7.2-memcached
    fi
  fi

  echo -n "Configure PHP7.2 ... "

  TIME_ZONE=$(cat /etc/timezone)

  sed -i 's/short_open_tags=Off/short_open_tags=On/' /etc/php/7.2/fpm/php.ini
  sed -i 's/;highlight./highlight/' /etc/php/7.2/fpm/php.ini
  sed -i 's/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE/' /etc/php/7.2/fpm/php.ini
  sed -i 's/enable_dl = Off/enable_dl = On/' /etc/php/7.2/fpm/php.ini
  sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.2/fpm/php.ini
  sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 25M/' /etc/php/7.2/fpm/php.ini
  sed -i "s/;date.timezone =/date.timezone=\"${TIME_ZONE//\//\\/}\"/" /etc/php/7.2/fpm/php.ini

  cp /etc/php/7.2/fpm/php.ini /etc/php/7.2/cli/.

  echo -e "[${green}DONE${NC}]\n"

}