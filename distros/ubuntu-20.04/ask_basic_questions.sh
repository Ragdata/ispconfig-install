#-------------------------------------------------------------------
# distros/ubuntu-20.04/ask_basic_questions.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/ask_basic_questions.sh
# Author:       Ragdata
# Date:         13/01/2021 1526
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
AskBasicQuestions()
{
  CFG_MULTISERVER=No
  CFG_WEBMAIL=RoundCube
  OpeningDialog
  exitstatus=$?
  if [ $exitstatus == 0 ]; then
    BasicSQLServerQuestion
  else
    exit 1
  fi
}

BasicSQLServerQuestion()
{
  SQLServerDialog
  exitRedirect BasicSQLServerPasswordQuestion AskBasicQuestions $?
}

BasicSQLServerPasswordQuestion()
{
  SQLServerPasswordDialog
  exitRedirect BasicWebServerQuestion BasicSQLServerQuestion $?
}

BasicWebServerQuestion()
{
  WebServerDialog
  exitRedirect BasicPHPCacheQuestion BasicSQLServerPasswordQuestion $?
}

BasicWebDAVQuestion()
{
  WebDAVDialog
  exitRedirect BasicPHPCacheQuestion BasicWebServerQuestion $?
}

BasicPHPCacheQuestion()
{
  PHPCacheDialog
  exitRedirect BasicMTAQuestion BasicWebDAVQuestion $?
}

BasicMTAQuestion()
{
  MTADialog
  exitRedirect BasicMailmanQuestion BasicPHPCacheQuestion $?
}

BasicMailmanQuestion()
{
  MailmanDialog
  exitRedirect BasicAVUpdateQuestion BasicMTAQuestion $?
}

BasicAVUpdateQuestion()
{
  AVUpdateDialog
  exitRedirect BasicQuotaQuestion BasicMTAQuestion $?
}

BasicQuotaQuestion()
{
  QuotaDialog
  exitRedirect BasicJailkitQuestion BasicAVUpdateQuestion $?
}

BasicJailkitQuestion()
{
  JailkitDialog
  exitRedirect BasicISPCQuestion BasicQuotaQuestion $?
}

BasicISPCQuestion()
{
  ISPCDialog
  exitRedirect BasicPHPMyAdminQuestion BasicJailkitQuestion $?
}

BasicPHPMyAdminQuestion()
{
  PHPMyAdminDialog
  exitRedirect BasicCertbotQuestion BasicISPCQuestion $?
}

BasicCertbotQuestion()
{
  CertbotDialog
  exitRedirect BasicSSLCountryQuestion BasicPHPMyAdminQuestion $?
}

BasicSSLCountryQuestion()
{
  SSLCountryDialog
  exitRedirect BasicSSLStateQuestion BasicPHPMyAdminQuestion $?
}

BasicSSLStateQuestion()
{
  SSLStateDialog
  exitRedirect BasicSSLLocalityQuestion BasicSSLCountryQuestion $?
}

BasicSSLLocalityQuestion()
{
  SSLLocalityDialog
  exitRedirect BasicSSLOrganizationQuestion BasicSSLStateQuestion $?
}

BasicSSLOrganizationQuestion()
{
  SSLOrganizationDialog
  exitRedirect BasicSSLOrgUnitQuestion BasicSSLLocalityQuestion $?
}

BasicSSLOrgUnitQuestion()
{
  SSLOrgUnitDialog
  exitRedirect InstallPackages BasicSSLOrganizationQuestion $?
}