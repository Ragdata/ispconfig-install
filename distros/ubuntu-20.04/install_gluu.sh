#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_gluu.sh
#-------------------------------------------------------------------
# ISPConfig 3+ AutoInstaller
#
# File:         distros/ubuntu-20.04/install_gluu.sh
# Author:       Ragdata
# Date:         13/01/2021 1853
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallGluu() {

  echo "deb https://repo.gluu.org/ubuntu/ focal main" > /etc/apt/sources.list.d/gluu-repo.list
  curl https://repo.gluu.org/ubuntu/gluu-apt.key | apt-key add -

  apt update
  apt install gluu-server
  apt-mark hold gluu-server

  /sbin/gluu-serverd enable
  /sbin/gluu-serverd start
  /sbin/gluu-serverd login

  cd /install/community-edition-setup
  ./setup.py

}

BackupGluu() {

  /sbin/gluu-serverd disable
  /sbin/gluu-serverd stop
  apt remove gluu-server
  rm -fr /opt/gluu-server.save

}

StartGluu() {

  /sbin/gluu-serverd enable
  /sbin/gluu-serverd start
  /sbin/gluu-serverd login

}

StopGluu() {

  /sbin/gluu-serverd disable
  /sbin/gluu-serverd stop

}

RestartGluu() {

  /sbin/gluu-serverd disable
  /sbin/gluu-serverd stop

  /sbin/gluu-serverd enable
  /sbin/gluu-serverd start
  /sbin/gluu-serverd login

}