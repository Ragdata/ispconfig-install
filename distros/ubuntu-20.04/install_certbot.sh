#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_certbot.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_certbot.sh
# Author:       Ragdata
# Date:         13/01/2021 1938
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallCertbot() {

  if [ "$CFG_WEBSERVER" == "Apache" ]; then

    apt install -y certbot

#    pkg_install snapd
#    snap install core
#    snap refresh core
#    apt remove certbot
#
#    snap install --classic certbot
#    ln -s /snap/bin/certbot /usr/bin/certbot
#
    certbot --apache
#    certbot renew --dry-run

  elif [ "$CFG_WEBSERVER" == "Nginx" ]; then

    apt install -y certbot

#    pkg_install snapd
#
#    snap install core
#    snap refresh core
#
#    apt remove certbot
#
#    snap install --classic certbot
#    ln -s /snap/bin/certbot /usr/bin/certbot

    certbot --nginx

  fi

}