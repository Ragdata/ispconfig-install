#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_sql.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_sql.sh
# Author:       Ragdata
# Date:         13/01/2021 1803
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallSQLServer() {

  if [ "$CFG_SQLSERVER" == "MySQL" ]; then

    echo -n "Installing Database Server (MySQL) ... "
    echo "mysql-server-5.5 mysql-server/root_password password $CFG_MYSQL_ROOT_PWD" | debconf-set-selections
    echo "mysql-server-5.5 mysql-server/root_password_again password $CFG_MYSQL_ROOT_PWD" | debconf-set-selections
    apt install -y mysql-client mysql-server
    sed -i 's/bind-address    = 127.0.0.1/#bind-address   = 127.0.0.1/' /etc/mysql/my.cnf
    echo -e "[${green}DONE${NC}]\n"

    echo -n "Restarting MySQL ... "
    service mysql restart
    echo -e "[${green}DONE${NC}]\n"

  elif [ "$CFG_SQLSERVER" == "MariaDB" ]; then

    echo -n "Installing Database Server (MariaDB) ... "
    echo "mysql-server-5.5 mysql-server/root_password password $CFG_MYSQL_ROOT_PWD" | debconf-set-selections
    echo "mysql-server-5.5 mysql-server/root_password_again password $CFG_MYSQL_ROOT_PWD" | debconf-set-selections
    apt install -y mariadb-client mariadb-server
    sed -i 's/bind-address    = 127.0.0.1/#bind-address   = 127.0.0.1/' /etc/mysql/mariadb.conf.d/50-server.cnf
    echo -e "[${green}DONE${NC}]\n"

    echo -n "Restarting MariaDB ... "
    service mysql restart
    echo -e "[${green}DONE${NC}]\n"

  fi

}