#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_nextcloud.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_nextcloud.sh
# Author:       Ragdata
# Date:         13/01/2021 1906
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallNextCloud()
{
  echo -n "Installing NextCloud ... "

  echo -e "${yellow}NOT YET IMPLEMENTED${NC}"
}