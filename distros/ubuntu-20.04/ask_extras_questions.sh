#-------------------------------------------------------------------
# distros/ubuntu-20.04/ask_extras_questions.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/ask_extras_questions.sh
# Author:       Ragdata
# Date:         13/01/2021 1526
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
AskExtraQuestions()
{
  OpeningDialog
  exitstatus=$?
  if [ $exitstatus == 0 ]; then
    ExtraHHVMQuestion
  else
    exit 1
  fi
}

ExtraHHVMQuestion()
{
  HHVMDialog
  exitRedirect ExtraXMPPQuestion AskExtraQuestions $?
}

ExtraXMPPQuestion()
{
  XMPPDialog
  exitRedirect ExtraMemDataQuestion ExtraHHVMQuestion $?
}

ExtraMemDataQuestion()
{
  MemDataDialog
  exitRedirect ExtraSSOQuestion ExtraXMPPQuestion $?
}

ExtraSSOQuestion()
{
  SSODialog
  exitRedirect ExtraPasswordVaultQuestion ExtraMemDataQuestion $?
}

ExtraPasswordVaultQuestion()
{
  PasswordVaultDialog
  exitRedirect ExtraNextCloudQuestion ExtraSSOQuestion $?
}

ExtraNextCloudQuestion()
{
  NextCloudDialog
  exitRedirect ExtraSSLCountryQuestion ExtraPasswordVaultQuestion $?
}

ExtraSSLCountryQuestion()
{
  SSLCountryDialog
  exitRedirect ExtraSSLStateQuestion ExtraNextCloudQuestion $?
}

ExtraSSLStateQuestion()
{
  SSLStateDialog
  exitRedirect ExtraSSLLocalityQuestion ExtraSSLCountryQuestion $?
}

ExtraSSLLocalityQuestion()
{
  SSLLocalityDialog
  exitRedirect ExtraSSLOrganizationQuestion ExtraSSLStateQuestion $?
}

ExtraSSLOrganizationQuestion()
{
  SSLOrganizationDialog
  exitRedirect ExtraSSLOrgUnitQuestion ExtraSSLLocalityQuestion $?
}

ExtraSSLOrgUnitQuestion()
{
  SSLOrgUnitDialog
  exitRedirect InstallPackages ExtraSSLOrganizationQuestion $?
}