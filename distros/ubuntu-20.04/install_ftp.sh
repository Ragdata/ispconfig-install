#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_ftp.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_ftp.sh
# Author:       Ragdata
# Date:         13/01/2021 1837
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallFTP() {
  echo -n "Installing FTP server (Pure-FTPd)... "
  echo "pure-ftpd-common pure-ftpd/virtualchroot boolean true" | debconf-set-selections
  apt install -y pure-ftpd-common pure-ftpd-mysql
  sed -i 's/ftp/\#ftp/' /etc/inetd.conf
  echo 1 > /etc/pure-ftpd/conf/TLS
  mkdir -p /etc/ssl/private/
  openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/ssl/private/pure-ftpd.pem -out /etc/ssl/private/pure-ftpd.pem -subj "/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCALITY/O=$SSL_ORGANIZATION/OU=$SSL_ORGUNIT/CN=$CFG_HOSTNAME_FQDN"
  chmod 600 /etc/ssl/private/pure-ftpd.pem
  echo -e "[${green}DONE${NC}]\n"
  echo -n "Restarting Pure-FTPd... "
  service openbsd-inetd restart
  service pure-ftpd-mysql restart
  echo -e "[${green}DONE${NC}]\n"
}