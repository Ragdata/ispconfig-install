#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_basics.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_basics.sh
# Author:       Ragdata
# Date:         13/01/2021 0828
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallBasics() {

  if [ ! -s /etc/apt/sources.list.d/ondrej-ubuntu-php-$REL.list ]; then
    echo -n "Adding repository for PHP ... "
    hide_output add-apt-repository -y ppa:ondrej/php
    echo -e "[${green}DONE${NC}]\n"
  fi

  if [ "$CFG_WEBSERVER" == "Apache" ]; then
    if [ ! -s /etc/apt/sources.list.d/ondrej-ubuntu-apache2-$REL.list ]; then
      echo -n "Adding repository for Apache2 ... "
      hide_output add-apt-repository -y ppa:ondrej/apache2
      echo -e "[${green}DONE${NC}]\n"
    fi
  else
    if [ ! -s /etc/apt/sources.list.d/ondrej-ubuntu-nginx-$REL.list ]; then
      echo -n "Adding repository for Nginx ... "
      hide_output add-apt-repository -y ppa:ondrej/nginx
      echo -e "[${green}DONE${NC}]\n"
    fi
  fi

  echo -n "Updating apt package database and upgrading currently installed packages ... "
  hide_output apt update
  hide_output apt dist-upgrade -y
  hide_output apt autoremove -y
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Installing basic packages (OpenSSH Server, NTP, BinUtils, etc.) ... "
  apt install -y rkhunter debconf-utils binutils php7.4-cli php-pear openssl-blacklist
  apt install -y python3.9 python3-pip software-properties-common mcrypt imagemagick curl tidy
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Stopping AppArmor ... "
  service apparmor stop
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Disabling AppArmor ... "
  hide_output update-rc.d -f apparmor remove
  echo -e "[${green}DONE${NC}]\n"

  echo -n "Removing AppArmor ... "
  pkg_remove apparmor apparmor-utils
  echo -e "[${green}DONE${NC}]\n"

  if [ /bin/sh -ef /bin/dash ]; then
    echo -n "Changing the default shell from dash to bash ... "
    echo "dash dash/sh boolean false" | debconf-set-selections
    dpkg-reconfigure -f noninteractive dash > /dev/null 2>&1
    echo -e "[${green}DONE${NC}]\n"
  fi
}