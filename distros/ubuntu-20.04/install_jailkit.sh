#-------------------------------------------------------------------
# distros/ubuntu-20.04/install_jailkit.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         distros/ubuntu-20.04/install_jailkit.sh
# Author:       Ragdata
# Date:         13/01/2021 1847
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
InstallJailKit() {

  echo -n "Installing Jailkit... "
  apt install -y jailkit
  echo -e "[${green}DONE${NC}]\n"

}