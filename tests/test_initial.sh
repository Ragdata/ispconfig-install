#-------------------------------------------------------------------
# tests/test_initial.sh
#-------------------------------------------------------------------
# ISPConfig 3+ Installer
#
# File:         tests/test_initial.sh
# Author:       Ragdata
# Date:         18/01/2021 0117
# License:      GNU GPLv3
#
# Attributions: Substantially based upon the work of Matteo Temporini and
#               the community supporting ispconfig_setup:
#
#               https://github.com/servisys/ispconfig_setup
#-------------------------------------------------------------------
export IS_TEST=true
bash ../install.sh